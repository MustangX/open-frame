<?php
/**
 * @package       OpenFrame
 * @since         0.0.1
 * @version       0.0.1
 * @copyright     2017 JMColeman <hyperclock@mustangx.org>
 * @license       MIT License
**/

namespace OpenFrame\Controllers;

use Http\Request;
use Http\Response;
use OpenFrame\Template\Renderer;

class Homepage
{
    private $request;
    private $response;
    private $renderer;

    public function __construct(
        Request $request,
        Response $response,
        Renderer $renderer
    ) {
        $this->request = $request;
        $this->response = $response;
        $this->renderer = $renderer;
    }

    public function show()
    {
        $data = [
            'name' => $this->request->getParameter('name', 'stranger'),
        ];
        $html = $this->renderer->render('Homepage', $data);
        $this->response->setContent($html);
    }
}

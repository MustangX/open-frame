<?php
/**
 * @package       OpenFrame
 * @since         0.0.1
 * @version       0.0.1
 * @copyright     2017 JMColeman <hyperclock@mustangx.org>
 * @license       MIT License
**/

namespace OpenFrame\Template;

interface Renderer
{
    public function render($template, $data = []) : string;
}

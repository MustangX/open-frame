<?php
/**
 * @package       OpenFrame
 * @since         0.0.1
 * @version       0.0.1
 * @copyright     2017 JMColeman <hyperclock@mustangx.org>
 * @license       MIT License
**/

namespace OpenFrame\Template;

use Twig_Environment;

class TwigRenderer implements Renderer
{
    private $renderer;

    public function __construct(Twig_Environment $renderer)
    {
        $this->renderer = $renderer;
    }

    public function render($template, $data = []) : string
    {
        return $this->renderer->render("$template.html.twig", $data);
    }
}
